import * as React from 'react';
import { Button, Platform, TextInput, View } from 'react-native';
import { NavigationScreenProps, NavigationState } from 'react-navigation';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { saveFeedUrl, getFeedUrl } from '$redux/reduxBundle';
import { ApplicationState } from '$redux/types';

import * as colors from '$common/colors';
import styles from './styles';

interface Props extends NavigationScreenProps<NavigationState> {
  feedUrl: string | null;
  saveFeedUrl(feedUrl: string): void;
}

interface State {
  feedUrl?: string
}

export class Setup extends React.Component<Props, State> {
  state = {
    feedUrl:
      this.props.feedUrl ||
      (__DEV__ ? `http://${Platform.OS === 'ios' ? 'localhost' : '10.0.2.2'}:9000/feed.json` : undefined)
  };

  protected setFeedUrl = () => {
    this.props.saveFeedUrl(this.state.feedUrl!);
    this.props.navigation.goBack();
  }

  public render() {
    return (
      <View style={styles.container}>
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.input}
            value={this.state.feedUrl}
            autoCapitalize={'none'}
            autoCorrect={false}
            onChangeText={feedUrl => this.setState(state => ({ ...state, feedUrl }))}
            autoFocus
            placeholder={'Enter feed URL here...'}
            placeholderTextColor={colors.textLighter}
          />
          <Button onPress={this.setFeedUrl} title={'Load'} color={colors.buttonPrimary}/>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state: ApplicationState) => ({
  feedUrl: getFeedUrl(state)
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  saveFeedUrl: (feedUrl: string) => dispatch(saveFeedUrl(feedUrl)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Setup)
