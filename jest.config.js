const transformIgnoredDirs = [
  'react-native-vector-icons',
  'react-native',
  'react-navigation',
  'react-navigation-tabs',
  'react-navigation-redux-helpers',
  'react-native-safari-view',
  'react-native-linear-gradient'
];

module.exports = {
  preset: 'react-native',
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.(js|ts)x?$',
  transform: {
    '^.+\\.(js|tsx?)$':
      '<rootDir>/node_modules/react-native/jest/preprocessor.js'
  },
  testPathIgnorePatterns: ['\\.snap$', '<rootDir>/node_modules/'],
  cacheDirectory: '.jest/cache',
  transformIgnorePatterns: [
    `node_modules/(?!(${transformIgnoredDirs.join('|')})/)`
  ]
};
