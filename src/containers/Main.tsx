import * as React from 'react';
import { Button, View } from 'react-native';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import styles from './styles';

import * as colors from '$common/colors';

import { NavigationScreenProps, NavigationState } from 'react-navigation';
import { SetupSourceRouteSceneName } from '$navigation/names';
import Feed from '$components/Feed';
import {
  getBookmarks,
  getFeedUrl,
  getViewed,
  markAsViewed,
  toggleBookmark,
  resetFeedUrl
} from '$redux/reduxBundle';
import { ApplicationState } from '$redux/types';

interface Props extends NavigationScreenProps<NavigationState> {
  feedUrl: string | null;
  viewedArticleKeys: string[];
  bookmarks: string[];
  markAsViewed(articleKey: string): void
  toggleBookmark(articleKey: string): void
  resetFeedUrl(): void
}

export class Main extends React.Component<Props> {
  private goToSetup = () => this.props.navigation.navigate(SetupSourceRouteSceneName);

  protected markIfNotViewed = (articleKey: string) => {
    if (this.props.viewedArticleKeys.indexOf(articleKey) === -1) {
      this.props.markAsViewed(articleKey);
    }
  }

  public render() {
    return (
      <View style={styles.container}>
        <View style={styles.feedContainer}>
          <Feed
            feedUrl={this.props.feedUrl}
            viewedArticleKeys={this.props.viewedArticleKeys}
            bookmarks={this.props.bookmarks}
            onOpen={this.markIfNotViewed}
            onLoadError={this.props.resetFeedUrl}
            onBookmarkIconPress={this.props.toggleBookmark}
          />
        </View>
        <View style={styles.setupContainer}>
          <Button title={'Setup the source'} onPress={this.goToSetup} color={colors.buttonPrimary}/>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state: ApplicationState) => ({
  feedUrl: getFeedUrl(state),
  viewedArticleKeys: getViewed(state),
  bookmarks: getBookmarks(state)
});

const markDispatchToProps = (dispatch: Dispatch) => ({
  resetFeedUrl: () => dispatch(resetFeedUrl()),
  markAsViewed: (articleKey: string) => dispatch(markAsViewed(articleKey)),
  toggleBookmark: (articleKey: string) => dispatch(toggleBookmark(articleKey))
});

export default connect(mapStateToProps, markDispatchToProps)(Main)
