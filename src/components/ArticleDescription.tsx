import * as React from 'react';
import { Alert, Button, Linking, View } from 'react-native';
import { WebView } from 'react-native-webview'
import { Article as ArticleItem } from '$common/types';
import * as colors from '$common/colors';
import * as utils from '$common/utils';
import styles from './styles';

interface IProps {
  article: ArticleItem
}

const createOpenUrl = (url: string) => async () => {
  try {
    await Linking.canOpenURL(url).then(async supported => {
      if (supported) {
        await Linking.openURL(url);
      } else {
        Alert.alert('', `Don\'t know how to open URI: ${url}`);
      }
    });
  } catch (err) {
    Alert.alert('', `Can\'t open / incorrect URI: ${url}\nOriginal error message: ${err.message}`);
  }
};


export default class ArticleDescription extends React.PureComponent<IProps> {
  state = { webViewOpacity: 0 };

  protected showWebView = () => this.setState(state => ({ ...state, webViewOpacity: 1 }));

  protected renderLinkButton() {
    return (
      <Button title={'GO'} onPress={createOpenUrl(this.props.article.link!)} color={colors.buttonPrimary}/>
    )
  }

  public render() {
    const { article } = this.props;
    const showButton = article.link && article.link.length;
    return (
      <>
        <View style={showButton ? styles.articleDescriptionContainer : styles.articleDescriptionContainerFullsize }>
          <WebView
            originWhitelist={['*']}
            style={[styles.articleDescription, { opacity: this.state.webViewOpacity }]}
            source={{ html: article.description ? utils.formatHtml(article.description) : '' }}
            onLoad={this.showWebView}
          />
        </View>
        {showButton ? this.renderLinkButton(): null}
      </>
    )
  }
}
