import * as React from 'react';
import { TouchableOpacity } from 'react-native';
import { NavigationScreenProps, NavigationState, withNavigation } from 'react-navigation';
import { ArticleDetailsRouteSceneName } from '$navigation/names';
import { ArticleFeedItem } from '$common/types';
import ArticleContent from '$components/ArticleContent';

const createOnPress =
  (article: ArticleFeedItem, navigation: any, itemOnPress: (itemKey: string) => void, isViewed: boolean) =>
    () => {
      itemOnPress(article.key);
      navigation.navigate(ArticleDetailsRouteSceneName, { article, isViewed });
    }

interface Props extends NavigationScreenProps<NavigationState> {
  article: ArticleFeedItem;
  isViewed: boolean;
  isBookmarked: boolean;
  onPress(itemKey: string): void;
  onBookmarkIconPress(itemKey: string): void;
}

export class FeedItem extends React.PureComponent<Props> {
  public render() {
    const { article, isBookmarked, isViewed, navigation, onPress } = this.props;
    return (
      <TouchableOpacity onPress={createOnPress(article, navigation, onPress, isViewed)}>
        <ArticleContent
          article={this.props.article}
          isViewed={isViewed}
          isBookmarked={isBookmarked}
          onBookmarkIconPress={this.props.onBookmarkIconPress}
        />
      </TouchableOpacity>
    )
  }
}

export default withNavigation(FeedItem);
