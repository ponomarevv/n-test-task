import * as constants from './colors';
import { isNaN } from 'lodash';

const padLeft = (str: string, count: number, character: string = '0') => str.padStart(count, character);

export const formatDate = (dateStr: string) => {
  const pubDate = new Date(Date.parse(dateStr));
  const [day, month, year, hour, minute] = [
    padLeft(`${pubDate.getDate()}`, 2),
    padLeft(`${pubDate.getMonth()}`, 2),
    pubDate.getFullYear(),
    padLeft(`${pubDate.getHours()}`, 2),
    padLeft(`${pubDate.getMinutes()}`, 2)
  ];

  return ![day, month, year, hour, minute].some(isNaN)
    ? `Published at ${day}/${month}/${year} at ${hour}:${minute}`
    : ''
}
export const formatHtml = (html: string): string =>
  `<body style="background-color: ${constants.bgPrimary}; font-size: 2em">${html}</body>`;

const htmlTagRegex = /(<([^>]+)>)/ig;
export const stripTags = (html: string): string => html.replace(htmlTagRegex, '');
