import { StyleSheet } from 'react-native';
import * as colors from '$common/colors';
import { normalize } from '$common/metrics';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-around',
    backgroundColor: colors.bgPrimary,
  },
  feedContainer: {
    flex: 0.85,
    paddingVertical: normalize(5),
  },
  articleDetailsContainer: {
    flex: 1,
    paddingVertical: normalize(5),
  },
  setupContainer: {
    flex: 0.15,
    alignItems: 'center',
    justifyContent: 'center'
  },
  inputContainer: {
    flexDirection: 'row',
    paddingHorizontal: normalize(10),
    justifyContent: 'space-around'
  },
  input: {
    flex: 0.7,
    minWidth: '45%'
  }
});
