import { Action } from 'redux';

export interface FluxAction<T> extends Action<T> {
  error?: boolean;
  meta?: any;
  payload?: { [key: string]: any };
}

export interface ApplicationState {
  feedUrl: string | null;
  viewed: string[];
  bookmarks: string[];
}

