export type Article = {
  date: string;
  description: string;
  imageUrl: string;
  link?: string;
  shortDescription: string;
  title: string;
}

export interface ArticleFeedItem extends Article {
  key: string;
}

export type Feed = {
  articles: Article[]
}
