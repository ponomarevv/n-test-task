import { StyleSheet } from 'react-native';
import * as colors from '$common/colors';
import * as constants from '$common/constants';
import { normalize, isSmallDevice } from '$common/metrics';

export default StyleSheet.create({
  emptyFeed: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
    minHeight: '100%'
  },
  article: {
    paddingVertical: normalize(5),
    paddingHorizontal: normalize(10),
    flexDirection: 'row',
    alignItems: 'flex-start'
  },
  articleEyeIcon: {
    flex: 1,
    position: 'absolute',
    right: normalize(20)
  },
  articleBookmarkIcon: {
    flex: 1,
    position: 'absolute',
    right: normalize(40)
  },
  articleImageContainer: {
    width: normalize(80),
    height: normalize(80),
    marginRight: normalize(10)
  },
  articleImage: {
    width: normalize(80),
    height: normalize(80),
    borderColor: colors.imageBorder,
    borderWidth: isSmallDevice ? normalize(2) : normalize(1)
  },
  articleContentContainer: {
    flex: 1,
    minHeight: normalize(80)
  },
  articleTitle: {
    color: colors.textPrimary,
    fontWeight: 'bold',
    fontSize: constants.baseFontSize,
    lineHeight: constants.baseLineHeight
  },
  articleDate: {
    color: colors.textLighter,
    marginVertical: normalize(5)
  },
  articleShortDescription: {
    color: colors.textPrimary
  },
  articleDescription: {
    backgroundColor: colors.bgPrimary,
    marginHorizontal: normalize(10)
  },
  articleDescriptionContainer: {
    flex: 0.95
  },
  articleDescriptionContainerFullsize: {
    flex: 1
  }
})
