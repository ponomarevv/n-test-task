import { createAppContainer } from 'react-navigation';
import rootStackNavigator from './navigators/rootStackNavigator';

export default createAppContainer(rootStackNavigator);
