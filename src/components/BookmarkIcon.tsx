import * as React from 'react';
import { TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import * as constants from '$common/constants';
import * as colors from '$common/colors';
import { normalize } from '$common/metrics';

interface Props {
  isBookmarked: boolean;
  articleKey: string;
  onPress(itemKey: string): void;
}

const createOnPress = (iconOnPressProp: (itemKey: string) => void, itemKey: string) => () => iconOnPressProp(itemKey);

const BookmarkIcon: React.FunctionComponent<Props> = (props) => (
  <TouchableOpacity onPress={createOnPress(props.onPress, props.articleKey)}>
    <Icon
      name={props.isBookmarked ? 'bookmark' : 'bookmark-plus-outline' }
      size={normalize(constants.iconFontSize)}
      color={props.isBookmarked ? colors.textPrimary : colors.textLighter}
    />
  </TouchableOpacity>
);

export default BookmarkIcon;
