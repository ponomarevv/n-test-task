import * as React from 'react';
import { Image, Text, View } from 'react-native';
import { ArticleFeedItem } from '$common/types';
import * as utils from '$common/utils';
import BookmarkIcon from './BookmarkIcon';
import EyeIcon from './EyeIcon';
import styles from './styles';

interface IProps {
  article: ArticleFeedItem
  showDate?: boolean,
  isViewed?: boolean
  isBookmarked?: boolean

  onBookmarkIconPress(itemKey: string): void
}

export default class ArticleContent extends React.PureComponent<IProps> {
  static defaultProps = {
    showDate: false,
    isViewed: false,
    isBookmarked: false,
  }

  public render() {
    const { article } = this.props;
    return (
      <View style={styles.article}>
        <View style={styles.articleImageContainer}>
          <Image
            resizeMode={'contain'}
            source={{ uri: article.imageUrl }}
            style={styles.articleImage}
          />
        </View>
        <View style={styles.articleContentContainer}>
          <Text style={styles.articleTitle}>{article.title && utils.stripTags(article.title)}</Text>
          {this.props.showDate ? (
            <Text style={styles.articleDate}>{article.date && utils.formatDate(article.date)}</Text>
          ) : null}
          <Text style={styles.articleShortDescription}>{article.shortDescription && utils.stripTags(article.shortDescription)}</Text>
        </View>
        <View style={styles.articleBookmarkIcon}>
          <BookmarkIcon
            isBookmarked={this.props.isBookmarked!}
            onPress={this.props.onBookmarkIconPress}
            articleKey={article.key}
          />
        </View>
        <View style={styles.articleEyeIcon}>
          <EyeIcon isViewed={this.props.isViewed!}/>
        </View>
      </View>
    )
  }
}
