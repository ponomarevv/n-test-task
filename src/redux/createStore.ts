import { AsyncStorage } from 'react-native';
import { createStore, compose } from 'redux';
import { persistStore, persistCombineReducers } from 'redux-persist';
import createFilter from 'redux-persist-transform-filter';
import { composeWithDevTools } from 'redux-devtools-extension';
import reducer, { appReducerKey } from './reduxBundle';

const composeEnhancer: any = __DEV__ ? composeWithDevTools : compose;
const saveBookmarksFilter = createFilter(appReducerKey, ['bookmarks']);

export default () =>  {

  const config = {
    key: 'root',
    whitelist: [appReducerKey],
    transforms: [saveBookmarksFilter],
    storage: AsyncStorage
  }

  const reducers = { [appReducerKey]: reducer };
  // @ts-ignore
  const persistedReducers = persistCombineReducers(config, reducers);
  const store = createStore(persistedReducers, composeEnhancer());
  const persistor = persistStore(store);

  return { store, persistor }
}
