module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['./src'],
        extensions: ['.js', '.jsx', '.ts', '.tsx', '.ios.js', '.android.js'],
        alias: {
          "$common": "./src/common",
          "$common/*": "./src/common/*",
          "$components": "./src/components",
          "$components/*": "./src/components/*",
          "$containers": "./src/containers",
          "$containers/*": "./src/containers/*",
          "$navigation": "./src/navigation",
          "$navigation/*": "./src/navigation/*",
          "$redux": "./src/redux",
          "$redux/*": "./src/redux/*",
        }
      }
    ],
    '@babel/plugin-transform-runtime'
  ],
  env: {
    production: {
      plugins: ['transform-remove-console']
    }
  }
};
