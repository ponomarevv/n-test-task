import * as React from 'react';
import { Alert, FlatList, Text, View, Dimensions } from 'react-native';
import { Article, ArticleFeedItem } from '$common/types';
import FeedItem from '$components/FeedItem';
import styles from './styles';
import md5 from 'md5';

const fetchFeed = async (
  url: string,
  onSuccess: (articles: Article[]) => void,
  onError: (error: Error) => void
) => {
  await fetch(url)
    .then(async response => {
      const respJson = await response.json();
      const articles = respJson.feed.article;
      onSuccess(articles);
    })
    .catch(e => onError(e));
};

const createKey = (article: Article, feedUrl: string) => md5(`${feedUrl}_${article.date}_${article.title}`);

const byDate = (ascending: boolean = true) => (a: Article, b: Article) =>
  ascending
    ? Date.parse(a.date) - Date.parse(b.date)
    : Date.parse(b.date) - Date.parse(a.date);

interface Props {
  feedUrl: string | null,
  viewedArticleKeys: string[],
  bookmarks: string[],
  onOpen(articleKey: string): void,
  onBookmarkIconPress(articleKey: string): void
  onLoadError(): void
}

interface State { articles: ArticleFeedItem[] }
interface RenderItemProps { item: ArticleFeedItem }

const EmptyFeed: React.FunctionComponent = () => (
  <View style={styles.emptyFeed}>
    <View>
      <Text>The source is not defined</Text>
    </View>
  </View>
);

const initialState: State = { articles: [] };

export default class Feed extends React.PureComponent<Props, State> {
  state = initialState;

  public componentDidMount() {
    if (this.props.feedUrl) {
      this.fetchFeed()
    }
  }

  public componentDidUpdate(prevProps: Props) {
    if (this.props.feedUrl && prevProps.feedUrl !== this.props.feedUrl) {
      this.fetchFeed()
    }
  }

  fetchFeed = () => {
    const { feedUrl } = this.props;
    if (feedUrl) {
      fetchFeed(
        feedUrl,
        (fetchedArticles) => {
          const rawArticles = (fetchedArticles as Article[]).sort(byDate(false));
          const articles = rawArticles.map(article => ({ ...article, key: createKey(article, feedUrl) }));
          this.setState(state => ({ ...state, articles }))
        },
        error => Alert.alert('Feed request error', error.message, [{ text: 'OK', onPress: this.onError }])
      )
    }
  }

  onError = () => {
    this.setState({ ...initialState });
    this.props.onLoadError()
  }

  public render() {
    return (
      <FlatList
        data={this.state.articles}
        extraData={this.props}
        renderItem={this.renderItem}
        keyExtractor={Feed.keyExtractor}
        ListEmptyComponent={EmptyFeed}
      />
    )
  }

  protected static keyExtractor = (item: ArticleFeedItem) => item.key;

  protected renderItem = ({ item }: RenderItemProps) => {
    const isBookmarked = this.props.bookmarks.indexOf(item.key) > -1;
    const isViewed = this.props.viewedArticleKeys.indexOf(item.key) > -1;
    return (
      <FeedItem
        article={item}
        isBookmarked={isBookmarked}
        isViewed={isViewed}
        onPress={this.props.onOpen}
        onBookmarkIconPress={this.props.onBookmarkIconPress}
      />
    )
  }
}
