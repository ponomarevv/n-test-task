import { union, difference } from 'lodash';

export const addItem = (collection: string[], articleKey: string) => union([], collection, [articleKey]);
export const removeItem = (collection: string[], articleKey: string) => difference(collection, [articleKey]);
export const includes = (collection: string[], articleKey: string) => collection.indexOf(articleKey) > -1;
export const toggle = (collection: string[], articleKey: string) =>
  includes(collection, articleKey) ? removeItem(collection, articleKey) : addItem(collection, articleKey)
