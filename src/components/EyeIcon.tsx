import * as React from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import * as constants from "$common/constants";
import * as colors from "$common/colors";
import { normalize } from 'common/metrics'

interface Props {
  isViewed: boolean;
}

const EyeIcon: React.FunctionComponent<Props> = (props) => (
  <Icon
    name={props.isViewed ? 'eye' : 'eye-outline' }
    size={normalize(constants.iconFontSize)}
    color={props.isViewed ? colors.textPrimary : colors.textLighter}
  />
);

export default EyeIcon;

