import { createStackNavigator } from 'react-navigation';
import { FeedRouteSceneName, ArticleDetailsRouteSceneName } from '$navigation/names'
import Main from '$containers/Main';
import ArticleDetails from '$containers/ArticleDetails';

export default createStackNavigator({
  [FeedRouteSceneName]: {
    screen: Main,
    navigationOptions: {
      title: 'Feed'
    }
  },
  [ArticleDetailsRouteSceneName]: {
    screen: ArticleDetails,
    navigationOptions: ({ navigation }) => ({
      title: navigation.state.params.article.title,
    })
  }
})
