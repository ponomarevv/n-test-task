// tslint:disable no-console
import * as fs from 'fs';
import http, { IncomingMessage, ServerResponse } from 'http';
import path from 'path';
import url from 'url';

let port: number = 9000;
let rootDir: string = `wwwroot`;

if (process.argv.length > 3) {
  rootDir = process.argv[3];
  port = parseInt(process.argv[2], 10);
} else {
  const maybePort = parseInt(process.argv[2], 10);
  if (!maybePort) {
    rootDir = process.argv[2];
  } else {
    port = maybePort;
  }
}

try {
  http.createServer((req: IncomingMessage, res: ServerResponse) => {
    console.log(`${req.method} ${req.url}`);

    // parse URL
    const parsedUrl = url.parse(req.url!);
    // extract URL path
    let pathname = `${parsedUrl.pathname ? decodeURIComponent(parsedUrl.pathname) : ''}`;

    // based on the URL path, extract the file extention. e.g. .js, .doc, ...
    let ext = path.parse(pathname).ext;
    // maps file extention to MIME type
    const map: {[key: string]: string} = {
      '.ico': 'image/x-icon',
      '.html': 'text/html',
      '.js': 'text/javascript',
      '.json': 'application/json',
      '.css': 'text/css',
      '.png': 'image/png',
      '.jpg': 'image/jpeg',
      '.wav': 'audio/wav',
      '.mp3': 'audio/mpeg',
      '.svg': 'image/svg+xml',
      '.pdf': 'application/pdf',
      '.doc': 'application/msword',
      '.xml': 'application/xml',
    };

    let filePath = path.resolve(path.join(__dirname, rootDir, pathname));

    fs.exists(filePath, (exist) => {
      if (!exist) {
        // if the file is not found, return 404
        res.statusCode = 404;
        res.end(`File ${pathname} not found!`);
        return;
      }

      // if is a directory search for index file matching the extention
      if (fs.statSync(filePath).isDirectory()) {
        ext = ext && ext.length ? ext : '.html';
        filePath += '/index' + ext
      }

      // read file from file system
      fs.readFile(filePath, (err, data) => {
        if (err) {
          res.statusCode = 500;
          res.end(`Error getting the file: ${err}.`);
        } else {
          // if the file is found, set Content-type and send data
          res.setHeader('Content-type', map[ext] || 'text/plain');
          res.end(data);
        }
      });
    });
  }).listen(port);
} catch (e) {
  console.error(e);
}
console.log(`Server listening on port ${port}`);
