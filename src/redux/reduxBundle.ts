import { get } from 'lodash';
import { ApplicationState, FluxAction } from './types';
import { addItem, toggle } from './utils'

export const appReducerKey = 'app';

export enum Action {
  SAVE_FEED_URL = 'SAVE_FEED_URL',
  RESET_FEED_URL = 'RESET_FEED_URL',
  MARK_AS_VIEWED = 'MARK_AS_VIEWED',
  TOGGLE_BOOKMARK = 'TOGGLE_BOOKMARK',
}

interface ApplicationAction<P> extends FluxAction<Action> {
  payload: P;
}

interface SaveFeedUrlAction extends ApplicationAction<{ feedUrl: string }> {}
interface ResetFeedUrlAction extends ApplicationAction<{ feedUrl: null }> {}
interface MarkAsViewedAction extends ApplicationAction<{ articleKey: string }> {}
interface ToggleBookmarkAction extends ApplicationAction<{ articleKey: string }> {}

export type AppAction =
  | SaveFeedUrlAction
  | ResetFeedUrlAction
  | MarkAsViewedAction
  | ToggleBookmarkAction

export const saveFeedUrl = (feedUrl: string) => ({ type: Action.SAVE_FEED_URL, payload: { feedUrl } });
export const resetFeedUrl = () => ({ type: Action.RESET_FEED_URL, payload: { feedUrl: null } });
export const getFeedUrl = (state: ApplicationState) => get(state, [appReducerKey, 'feedUrl']);

export const markAsViewed = (articleKey: string) => ({ type: Action.MARK_AS_VIEWED, payload: { articleKey } });
export const getViewed = (state: ApplicationState) => get(state, [appReducerKey, 'viewed']) || [];

export const toggleBookmark = (articleKey: string) => ({ type: Action.TOGGLE_BOOKMARK, payload: { articleKey } });
export const getBookmarks = (state: ApplicationState) => get(state, [appReducerKey, 'bookmarks']) || [];

const initialState: ApplicationState = { feedUrl: null, viewed: [], bookmarks: [] };
const reducer = (state: ApplicationState = initialState, action: AppAction) => (
  (({
    [Action.SAVE_FEED_URL]: (s: ApplicationState) => ({...s, ...action.payload }),
    [Action.RESET_FEED_URL]: (s: ApplicationState) => ({...s, ...action.payload }),
    // @ts-ignore
    [Action.MARK_AS_VIEWED]: (s: ApplicationState) => ({ ...s, viewed: addItem(s.viewed, action.payload.articleKey) }),
    // @ts-ignore
    [Action.TOGGLE_BOOKMARK]: (s: ApplicationState) => ({ ...s, bookmarks: toggle(s.bookmarks, action.payload.articleKey) })
  })[action.type]) || (s => s)
)(state);

export default reducer;
