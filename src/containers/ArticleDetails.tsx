import * as React from 'react';
import { View } from 'react-native';
import { NavigationScreenProps, NavigationState } from 'react-navigation';

import ArticleContent from '$components/ArticleContent';
import ArticleDescription from '$components/ArticleDescription';
import { ArticleFeedItem } from '$common/types';
import styles from './styles';

import { getBookmarks, getViewed, markAsViewed, toggleBookmark } from '$redux/reduxBundle';
import { includes } from '$redux/utils';

import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import { ApplicationState } from '$redux/types';

interface ArticleScreenNavigationParams {
  article: ArticleFeedItem;
}

interface Props extends NavigationScreenProps<NavigationState & ArticleScreenNavigationParams> {
  isViewed: boolean;
  isBookmarked: boolean;
  markAsViewed(itemKey: string): void;
  toggleBookmark(itemKey: string): void;
}

const ArticleDetails: React.FunctionComponent<Props> = (props) => {
    if (props.navigation.state.params) {
      const { article } = props.navigation.state.params;
      return (
        <View style={styles.container}>
          <View style={styles.articleDetailsContainer}>
            <ArticleContent
              article={article}
              showDate
              isViewed={props.isViewed}
              isBookmarked={props.isBookmarked}
              onBookmarkIconPress={props.toggleBookmark}
            />
            <ArticleDescription article={article}/>
          </View>
        </View>
      )
    }
    return null;
  };

const mapStateToProps = (state: ApplicationState, ownProps: any) => {
  let articleKey;

  if(ownProps.navigation.state.params) {
    articleKey = ownProps.navigation.state.params.article.key;
  }

  return {
    isViewed: articleKey && includes(getViewed(state), articleKey),
    isBookmarked: articleKey && includes(getBookmarks(state), articleKey)
  }
};

const markDispatchToProps = (dispatch: Dispatch) => ({
  markAsViewed: (articleKey: string) => dispatch(markAsViewed(articleKey)),
  toggleBookmark: (articleKey: string) => dispatch(toggleBookmark(articleKey))
});

export default connect(mapStateToProps, markDispatchToProps)(ArticleDetails)
