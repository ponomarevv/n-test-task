import { createStackNavigator } from 'react-navigation';
import { MainStackName, SetupSourceRouteSceneName } from '$navigation/names'
import feedStackNavigator from './feedStackNavigator';
import Setup from '$containers/Setup';

export default createStackNavigator({
  [MainStackName]: {
    screen: feedStackNavigator,
    navigationOptions: {
      title: 'Feed',
      header: null,
    }
  },
  [SetupSourceRouteSceneName]: {
    screen: Setup,
    navigationOptions: {
      title: 'Setup',
      header: null,
    }
  }
}, {
  mode: 'modal'
})
